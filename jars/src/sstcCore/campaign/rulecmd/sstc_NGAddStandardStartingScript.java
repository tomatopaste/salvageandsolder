package sstcCore.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.Script;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.CharacterCreationData;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.rulecmd.BaseCommandPlugin;
import com.fs.starfarer.api.util.Misc;

import java.util.List;
import java.util.Map;

public class sstc_NGAddStandardStartingScript extends BaseCommandPlugin {
    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        if (dialog == null) return false;

        CharacterCreationData data = (CharacterCreationData) memoryMap.get(MemKeys.LOCAL).get("$characterData");
        final MemoryAPI memory = memoryMap.get(MemKeys.LOCAL);

        data.addScript(new Script() {
            @Override
            public void run() {
                CampaignFleetAPI fleet = Global.getSector().getPlayerFleet();

                //add supplies, crew and fuel
                int crew = 0;
                int supplies = 0;
                for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
                    crew += Math.ceil(member.getMinCrew() + (member.getMaxCrew() - member.getMinCrew()) * 0.5f);
                    supplies += member.getCargoCapacity();
                }

                CargoAPI cargo = fleet.getCargo();
                cargo.addCrew(crew);
                cargo.addSupplies(supplies);
                cargo.addFuel(cargo.getMaxFuel());

                fleet.getFleetData().ensureHasFlagship();

                for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
                    float max = member.getRepairTracker().getMaxCR();
                    member.getRepairTracker().setCR(max);
                }
                fleet.getFleetData().setSyncNeeded();
            }
        });
        //return true;
        throw new ClassCastException("lol");
    }
}
