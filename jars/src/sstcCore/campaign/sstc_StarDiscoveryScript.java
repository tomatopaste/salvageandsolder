package sstcCore.campaign;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

public class sstc_StarDiscoveryScript extends BaseCampaignEventListener implements EveryFrameScript {
    public static final String STAR_DISCOVERY_MAP_KEY = "sstc_star_discovery";
    public static final float STAR_DISCOVERY_RADIUS = 200f;

    public static Logger log = Global.getLogger(sstc_StarDiscoveryScript.class);

    static final IntervalUtil tracker = new IntervalUtil(0.1f, 0.3f);

    public sstc_StarDiscoveryScript() {
        super(true);
    }

    //liberated from nexerilin's SectorManager.java
    public static sstc_StarDiscoveryScript create() {
        sstc_StarDiscoveryScript script = getDiscoveryScript();
        if (script != null) {
            return script;
        }

        Map<String, Object> data = Global.getSector().getPersistentData();
        script = new sstc_StarDiscoveryScript();
        data.put(STAR_DISCOVERY_MAP_KEY, script);
        return script;
    }

    //liberated from nexerilin's SectorManager.java
    public static sstc_StarDiscoveryScript getDiscoveryScript() {
        Map<String, Object> data = Global.getSector().getPersistentData();
        return (sstc_StarDiscoveryScript) data.get(STAR_DISCOVERY_MAP_KEY);
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }

    @Override
    public void advance(float amount) {
        tracker.advance(amount);
        SectorAPI sector = Global.getSector();
        GenericPluginManagerAPI plugins = sector.getGenericPlugins();
        sstc_DiscoverEntityPlugin plugin = (sstc_DiscoverEntityPlugin) plugins.getPluginsOfClass(sstc_DiscoverEntityPlugin.class).get(0);

        if (tracker.intervalElapsed() && sector.getPlayerFleet().isInHyperspace()) {
            List<SectorEntityToken> entities = sector.getHyperspace().getAllEntities();

            for (SectorEntityToken entity : entities) {
                if (entity instanceof JumpPointAPI) {
                    if (!((JumpPointAPI) entity).isStarAnchor()) {
                        continue;
                    }

                    if (entity.isDiscoverable() && entity.isVisibleToPlayerFleet()) {
                        log.info("Star Made Visible: " + entity.getName());

                        plugin.discoverEntity(entity);
                    }
                }
            }
        }
    }
}
