package sstcCore.campaign;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import org.apache.log4j.Logger;
import org.lwjgl.util.vector.Vector2f;
import sstcCore.world.sstc_SectorGenerationHandler;

public class sstc_Generate implements SectorGeneratorPlugin {
    public static Logger log = Global.getLogger(sstc_Generate.class);

    public static final Vector2f CORE_CENTER = new Vector2f(0, 0);

    @Override
    public void generate(SectorAPI sector) {
        log.info("Initialising SSTC generation...");

        new sstc_SectorGenerationHandler().generate(sector);

        initFactionRelationships(sector);
    }

    public static void initFactionRelationships(SectorAPI sector) {
        //todo - faction rep mixing
        FactionAPI hegemony = sector.getFaction(Factions.HEGEMONY);
        FactionAPI league = sector.getFaction(Factions.PERSEAN);
        FactionAPI independent = sector.getFaction(Factions.INDEPENDENT);
    }
}
