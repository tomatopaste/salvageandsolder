package sstcCore.plugins;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import sstcCore.campaign.sstc_Generate;
import sstcCore.campaign.sstc_DiscoverEntityPlugin;
import org.apache.log4j.Logger;
import sstcCore.campaign.sstc_StarDiscoveryScript;

public class sstc_ModPlugin extends BaseModPlugin {
    public static Logger log = Global.getLogger(sstc_ModPlugin.class);

    public static final String MOD_ID = "salvage_and_solder_tc";
    public static final String MOD_AUTHORS = "tomatopaste" + "...";
    public static final String MOD_ERROR_PREFIX =
            System.lineSeparator()
                    + System.lineSeparator() + MOD_ID + " by " + MOD_AUTHORS
                    + System.lineSeparator() + System.lineSeparator()
                    + "This wasn't supposed to happen..."
                    + System.lineSeparator();

    public static final boolean HideSystems = Global.getSettings().getBoolean("sstc_HideSystems");

    @Override
    public void onApplicationLoad() throws ClassNotFoundException {
        try {
            Global.getSettings().getScriptClassLoader().loadClass("org.lazywizard.lazylib.ModUtils");
        } catch (ClassNotFoundException ex) {
            String message = System.lineSeparator()
                    + System.lineSeparator() + "LazyLib is required to run at least one of the mods you have installed."
                    + System.lineSeparator() + System.lineSeparator()
                    + "You can download LazyLib at http://fractalsoftworks.com/forum/index.php?topic=5444"
                    + System.lineSeparator();
            throw new ClassNotFoundException(message);
        }
    }

    @Override
    public void onNewGame() {
        new sstc_Generate().generate(Global.getSector());
    }

    @Override
    public void onNewGameAfterProcGen() {
        if (HideSystems) {
            for (SectorEntityToken token : Global.getSector().getHyperspace().getAllEntities()) {
                //only if the star anchor jump point is hidden for a system, will the system not appear on map.
                if (token instanceof JumpPointAPI && ((JumpPointAPI) token).isStarAnchor()) {
                    log.info(" - Star Jump Point - Location: " + token.getLocation().toString());
                    token.setDiscoverable(true);
                    token.setSensorProfile(2000f);
                }
            }
        }
    }

    @Override
    public void onGameLoad(boolean isNewGame) {
        SectorAPI sector = Global.getSector();
        GenericPluginManagerAPI plugins = sector.getGenericPlugins();

        if (!plugins.hasPlugin(sstc_DiscoverEntityPlugin.class)) {
            plugins.addPlugin(new sstc_DiscoverEntityPlugin(), true);
        }

        addScripts();
    }

    public static void addScripts() {
        SectorAPI sector = Global.getSector();

        sector.addScript(sstc_StarDiscoveryScript.create());
    }
}
