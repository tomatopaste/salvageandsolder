package sstcCore.world.procgen;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorGenProgress;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import com.fs.starfarer.api.campaign.SectorProcGenPlugin;
import com.fs.starfarer.api.characters.CharacterCreationData;
import com.fs.starfarer.api.impl.campaign.procgen.MarkovNames;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;

public class sstc_ProcGenMod implements SectorProcGenPlugin {

    public static final float CELL_SIZE = Global.getSettings().getFloat("sstc_ConstellationSize");
    public static final int CONSTELLATION_CELLS = (int)Global.getSettings().getFloat("sstc_ConstellationCellSize");

    @Override
    public void prepare(CharacterCreationData data) {
        // do this here so that hand-crafted systems using
        // procgen use the proper seed
        if (data.getSeed() > 0) {
            StarSystemGenerator.random.setSeed(data.getSeed());
        }
        StarSystemGenerator.updateBackgroundPickers();

        MarkovNames.loadIfNeeded();
    }

    @Override
    public void generate(CharacterCreationData data, SectorGenProgress progress) {

    }
}
