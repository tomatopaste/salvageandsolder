package sstcCore.world;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import org.apache.log4j.Logger;
import org.lwjgl.util.vector.Vector2f;


public class sstc_SectorGenerationHandler implements SectorGeneratorPlugin {

    public static Logger log = Global.getLogger(sstcCore.world.sstc_SectorGenerationHandler.class);

    public static final Vector2f CORE_CENTER = new Vector2f(0, 0);

    static boolean doHyperstormGen = Global.getSettings().getBoolean("sstc_GenerateHyperstorms");

    @Override
    public void generate(SectorAPI sector) {
        log.info("Commencing SSTC sector generation...");

        //temp/debug stuff

        StarSystemAPI system = sector.createStarSystem("Corvus");
        system.setBackgroundTextureFilename("graphics/backgrounds/background4.jpg");

        sector.setRespawnLocation(system);
        sector.getRespawnCoordinates().set(-2500, -3500);

    }
}
